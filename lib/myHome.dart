import 'package:flutter/material.dart';

import 'package:flutter/foundation.dart';
import './nav/bottomNavigation.dart';
import './utils/storage.dart';

class MyHomePage extends StatefulWidget {
  final String userId;
  final String userType;
  MyHomePage({Key key,this.userId,this.userType});

  @override
  _MyHomePageState createState() => _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> {
  String folder = "JUSMS";
  @override
  void initState() {
    createDir(folder);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //return Tabs(userId: widget.userId,);
    return BottomNavigation(
      userType: widget.userType, 
      userId: widget.userId,
      rootPath: getRootPath(),
    );
  }
}