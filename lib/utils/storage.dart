import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'dart:async';

class Storage {
  String dirToBeCreated;

  Storage(String dirToBeCreated){
    this.dirToBeCreated = dirToBeCreated;
  }
}

Future<String> createDir(String dirToBeCreated) async {
  Directory baseDir = await getExternalStorageDirectory(); //only for Android
  String finalDir = join(baseDir.path, dirToBeCreated);
  
  var dir = Directory(finalDir);
  bool dirExists = await dir.exists();
  if(!dirExists){
    dir.create(/*recursive=true*/); //pass recursive as true if directory is recursive
  }else{
    print('created :: $dir');
  }
  return dir.toString();
}

String getRootPath(){
  return '/storage/emulated/0/JUSMS';
}

createNewFolder( String rootFolderPath, String currentPath, String title, String newFolderName) async {
  if(title.toString() == 'null')
    rootFolderPath = currentPath ;
  else
    rootFolderPath = currentPath + '/' + title;
  
  String dirToBeCreated = newFolderName;
  String finalDir = join(rootFolderPath+ '/', dirToBeCreated);
  
  var dir = Directory(finalDir);
  bool dirExists = await dir.exists();
  if(!dirExists){
    dir.create();
  }else{
    print('created :: $dir');
  }
}