import 'package:flutter/material.dart';

import '../FrontWidget/dateWidget.dart';
import '../FrontWidget/TempList.dart';
import 'frontFolder.dart';
import 'timeTable.dart';

class Dashboard extends StatelessWidget {
  final String rootPath;
  Dashboard({
    Key key,
    this.rootPath
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          DateWidget(),
          //NewsFeed(color: Colors.white),
          TempList(),
          TimeTable(),
          FrontFolder(currPath: rootPath),
        ],
      ),
    );
  }
}