import 'package:flutter/material.dart';

import '../notes/folder.dart';

class FrontFolder extends StatelessWidget {
  final String title;
  final String currPath;
  FrontFolder({
    Key key,
    this.title,
    this.currPath
  });

  @override
  Widget build(BuildContext context) {
    print('dash :: $currPath');
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Folder(
            iconSize: 40,
            folderName: "Study",
            heightFactor: 0.20,
            widthFactor: .45,
          ),
          Folder(
            iconSize: 40,
            folderName: "Questions",
            heightFactor: 0.20,
            widthFactor: .45,
          )
        ],
      ),
    );
  }
}